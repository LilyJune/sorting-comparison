from random import shuffle
from random import sample
from matplotlib import pyplot
import time


class SortStrategy:
    name = ""

    def __init__(self, func=None, type=""):
        if func:
            self.execute = func
            self.name = type

    def execute(self):
        print("No sorting algorithm selected")


def insert_sort(list_items):
    for i in range(1, len(list_items)):

        value = list_items[i]
        key = i

        while key > 0 and list_items[key - 1] > value:
            list_items[key] = list_items[key - 1]
            key -= 1

        list_items[key] = value


def merge_sort(list_items):
    if len(list_items) > 1:
        mid = len(list_items) // 2
        left_half = list_items[:mid]
        right_half = list_items[mid:]

        merge_sort(left_half)
        merge_sort(right_half)

        i = 0
        j = 0
        k = 0
        while i < len(left_half) and j < len(right_half):
            if left_half[i] < right_half[j]:
                list_items[k] = left_half[i]
                i += 1
            else:
                list_items[k] = right_half[j]
                j += 1
            k += 1

        while i < len(left_half):
            list_items[k] = left_half[i]
            i += 1
            k += 1

        while j < len(right_half):
            list_items[k] = right_half[j]
            j += 1
            k += 1


def hybrid_sort(list_items):
    if len(list_items) > 1:
        if len(list_items) < 97:
            insert_sort(list_items)
        else:
            mid = len(list_items) // 2
            left_half = list_items[:mid]
            right_half = list_items[mid:]

            hybrid_sort(left_half)
            hybrid_sort(right_half)

            i = 0
            j = 0
            k = 0
            while i < len(left_half) and j < len(right_half):
                if left_half[i] < right_half[j]:
                    list_items[k] = left_half[i]
                    i += 1
                else:
                    list_items[k] = right_half[j]
                    j += 1
                k += 1

            while i < len(left_half):
                list_items[k] = left_half[i]
                i += 1
                k += 1

            while j < len(right_half):
                list_items[k] = right_half[j]
                j += 1
                k += 1


def populate_list(items):
    return sample(range(items), items)


def randomizer(list_items):
    for i in range(0, len(list_items)):
        shuffle(list_items[i])


def measure(strat: SortStrategy, list_of_items, current, total):
    print(str(strat.name) + " Sort [" + str(current) + "/" + str(total) + "]")
    start = time.time()
    strat.execute(list_of_items)
    end = time.time()

    return end - start


def graph(values: list, times: list, symbol):
    pyplot.plot(values, times, symbol)


def measure_one_case(strat: SortStrategy, sizes: list, values: list, symbol):
    times = []
    for i in range(0, len(sizes)):
        total_time = measure(strat, values[i], i, len(sizes))
        times.append(total_time)
    print(times)
    graph(sizes, times, symbol)


def measure_all_one_case(sizes, values, title):
    measure_one_case(insert, sizes, values, "go")
    measure_one_case(merge, sizes, values, "ro")
    measure_one_case(hybrid, sizes, values, "b^")

    pyplot.title(str(title))
    pyplot.ylabel("time (s)")
    pyplot.xlabel("size of list")
    pyplot.show()


def generate_random(sizes):
    ret = []
    for i in range(0, len(sizes)):
        ret.append(populate_list(sizes[i]))
    return ret


def measure_random(sizes, values, title):
    measure_one_case(insert, sizes, values, "go")
    randomizer(values)
    measure_one_case(merge, sizes, values, "ro")
    randomizer(values)
    measure_one_case(hybrid, sizes, values, "b^")

    pyplot.title(str(title))
    pyplot.ylabel("time (s)")
    pyplot.xlabel("size of list")
    pyplot.show()


def generate_best_case(sizes):
    ret = []
    for i in range(0, len(sizes)):
        temp = list(range(sizes[i]))
        ret.append(temp)
    return ret


def generate_worst_case(sizes):
    ret = []
    for i in range(0, len(sizes)):
        temp = list(range(sizes[i]))
        temp.reverse()
        ret.append(temp)
    return ret


if __name__ == "__main__":

    insert = SortStrategy(insert_sort, "Insert")
    merge = SortStrategy(merge_sort, "Merge")
    hybrid = SortStrategy(hybrid_sort, "Hybrid")

    lengths = list(range(0, 500, 10))

    best_case = generate_best_case(lengths)
    random = generate_random(lengths)
    worst_case = generate_worst_case(lengths)

    print("WORST CASE")
    measure_all_one_case(lengths, worst_case, "WORST CASE")
    print("AVERAGE")
    measure_random(lengths, random, "AVERAGE")
    print("BEST CASE")
    measure_all_one_case(lengths, best_case, "BEST CASE")

